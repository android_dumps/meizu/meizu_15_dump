#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:21595436:4402e61e4b993cc6778f670950fe64f295e706af; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:14259496:0c9f82367b3decbc851b4864a13a6b2534eef13d EMMC:/dev/block/bootdevice/by-name/recovery 4402e61e4b993cc6778f670950fe64f295e706af 21595436 0c9f82367b3decbc851b4864a13a6b2534eef13d:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
